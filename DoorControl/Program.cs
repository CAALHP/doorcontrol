﻿using System;
using Phidgets;
using Phidgets.Events;

namespace DoorControl
{
    public class Program
    {
        //Declare an InterfaceKit object
        static InterfaceKit ifKit;
        
        public static void Main()
        {
            try
            {
                //Initialize the InterfaceKit object
                ifKit = new InterfaceKit();
                
                startUp();
                

                Console.WriteLine("Hit 'b' to open the door. \nHit 'c' to unlock the door.\nHit 'v' to unlock AND open the door.\nHit 'g' toggle Autolock-mode.(Default Off)"
                    + "\nHit 'x' to close the application");
                
                ConsoleKeyInfo ch;
                string char_input;
                // Waits for a 'b' to open the door.
              
             
            }
            catch (PhidgetException ex)
            {
                Console.WriteLine(ex.Description);
            }
        }

        public static void startUp()
        {
            //Hook the basica event handlers
            ifKit.Attach += new AttachEventHandler(ifKit_Attach);
            ifKit.Detach += new DetachEventHandler(ifKit_Detach);
            ifKit.Error += new ErrorEventHandler(ifKit_Error);


            //Open the object for device connections
            ifKit.open();


            //Wait for an InterfaceKit phidget to be attached
            Console.WriteLine("Waiting for InterfaceKit to be attached...");
            ifKit.waitForAttachment();
        }
        //Attach event handler...Display the serial number of the attached InterfaceKit 
        //to the console
        public static void ifKit_Attach(object sender, AttachEventArgs e)
        {
            Console.WriteLine("InterfaceKit {0} attached!",
                                e.Device.SerialNumber.ToString());
        }

        //Detach event handler...Display the serial number of the detached InterfaceKit 
        //to the console
        public static void ifKit_Detach(object sender, DetachEventArgs e)
        {
            Console.WriteLine("InterfaceKit {0} detached!",
                                e.Device.SerialNumber.ToString());
        }

        //Error event handler...Display the error description to the console
        public static void ifKit_Error(object sender, ErrorEventArgs e)
        {
            Console.WriteLine(e.Description);
        }

        // Sends an impulse to open the door
        public static void ifKit_OpenDoor()
        {
            ifKit.outputs[0] = true;
            System.Threading.Thread.Sleep(200);
            ifKit.outputs[0] = false;
            Console.WriteLine("\rOpening door...");
        }

        // Sends an umpulse to unlock the door
        public static void ifKit_ToggleLock()
        {
            ifKit.outputs[1] = true;
            System.Threading.Thread.Sleep(200);
            ifKit.outputs[1] = false;
            Console.WriteLine("\rUnlocking door...");
        }

        // Unlocks the door and opens it
        public static void ifKit_DoorAndLock()
        {
            if (ifKit.outputs[2] == true)
            {
                ifKit.outputs[1] = true;
                System.Threading.Thread.Sleep(200);
                ifKit.outputs[1] = false;
            }
            System.Threading.Thread.Sleep(500);
            ifKit.outputs[0] = true;
            System.Threading.Thread.Sleep(200);
            ifKit.outputs[0] = false;
            Console.WriteLine("\rUnlocking AND opening door...");
        }

        public static void ifKit_ToggleAutoLockMode()
        {
            if (ifKit.outputs[2] == true)
            {
                ifKit.outputs[2] = false;
                Console.WriteLine("\rAutolock-mode is OFF.");
            }
            else
            {
                ifKit.outputs[2] = true;
                Console.WriteLine("\rAutolock-mode is ON.");
            }
        }
    }
}
