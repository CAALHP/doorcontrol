﻿using System;
using System.Collections.Generic;
using caalhp.Core.Contracts;
using caalhp.IcePluginAdapters;
using DoorLib.Unity.SelfHostWebApiOwin;
using Microsoft.Practices.Unity;

namespace DoorLib
{
    public class DoorImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;

        public DoorImplementation()
        {
            DoorControl.Program.Main();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {

        }

        public string GetName()
        {
            return "Door Driver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            try
            {
                _host = hostObj;
                _processId = processId;
                //_host.SubscribeToEvents(_processId);
                Console.WriteLine("About to start the Unity framework and Owin selfhost");
                Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when activating Start() : " + e.Message);
            }

        }

        public void Start()
        {
            
            try
            {
                /*var unity = UnityHelpers.GetConfiguredContainer();
                
                unity.RegisterInstance(_host);
                unity.RegisterInstance(_processId);
                unity.RegisterType<DoorController>(new InjectionConstructor(_host, _processId));
                unity.RegisterInstance(this);
                unity.RegisterType<IDeviceDriverHostCAALHPContract, DeviceDriverHostToCAALHPContractAdapter>(
                    new PerResolveLifetimeManager());*/
                Startup.StartServer();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void Stop()
        {
            // Close the ServiceHost.
            //_server.Dispose();
        }
    }
}
