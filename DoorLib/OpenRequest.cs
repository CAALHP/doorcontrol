namespace DoorLib
{
    public class OpenRequest
    {
        public string Room {get;set;}
        public string Key { get; set; }
    }
}