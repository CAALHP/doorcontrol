﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoorLib
{
    public class DoorController : ApiController
    {
        //[POST("device")]
        public HttpResponseMessage OpenPost(OpenRequest req)
        {
            //implementation
            //profile must be sent in x-www-form-urlencoded format.
            //thanks to http://stackoverflow.com/questions/14624306/web-api-parameter-always-null
            Console.WriteLine("Call to DoorController.Post");
            var response = GetValidationResponse(req.Room, req.Key);
            if(response.IsSuccessStatusCode)
                DoorControl.Program.ifKit_OpenDoor();
            return response;
        }

        private static HttpResponseMessage GetValidationResponse(string room, string key)
        {
            var response = IsKeyValid(room, key) ? new HttpResponseMessage(HttpStatusCode.OK) : new HttpResponseMessage(HttpStatusCode.Unauthorized);
            return response;
        }

        private static bool IsKeyValid(string room, string key)
        {
            return room.Equals("339E") && key.Equals("1234");
        }
    }
}
