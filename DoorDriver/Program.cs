﻿using System;
using caalhp.IcePluginAdapters;
using DoorLib;

namespace DoorDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            const string endpoint = "localhost";
            Console.WriteLine("starting Crip2CaalhpService...");
            DeviceDriverAdapter adapter;
            var implementation = new DoorImplementation();
            try
            {
                adapter = new DeviceDriverAdapter(endpoint, implementation);
                Console.WriteLine("Crip2CaalhpService running");
            }
            catch (Exception ex)
            {
                //Connection to host probably failed
                Console.WriteLine(ex.Message);                
                implementation.Start();
            }
            Console.WriteLine("press <enter> to exit...");
            Console.ReadLine();
            
        }
    }
}
